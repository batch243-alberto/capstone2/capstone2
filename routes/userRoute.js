const express = require("express")

const router = express.Router()
const auth = require("../auth")

const userController = require("../controllers/userController");

// Controller starts
router.post("/registerUser", userController.registerUser);

router.post("/login", userController.loginUser);

router.patch("/updateRole/:userId", auth.verify, userController.updateRole)

router.post("/:productId/checkOut", auth.verify,userController.checkOut)

module.exports = router;
