const Product = require("../models/Product");
const User = require("../models/User");

const bcrypt = require("bcrypt")
const auth = require("../auth")

module.exports.addProduct = (request, response) =>{
	let newProduct = new Product({
		name: request.body.name,
		description: request.body.description,
		price: request.body.price,
		stock: request.body.stock,
	})
	const userData = auth.decode(request.headers.authorization)
	if(userData.isAdmin){
		return Product.find({name: request.body.name}).then(result =>{
			if(result.length >0){
				return response.send("This product was already registered.")
			}
			else{
				newProduct.save().then(prod =>{
				console.log(prod)
				return response.send(true)
				}).catch(error =>{
				console.log(error)
				})
			}
		})
	}
	else{
		return response.send("Your are not an admin")
	}
}

module.exports.allActiveProducts = (request, response) =>{

	return Product.find({isActive: true}).then(result => {
		return response.send(result)
		}).catch(error =>{
		console.log(error)
		})
}

module.exports.allProducts = (request, response) =>{
	const userData = auth.decode(request.headers.authorization);

	if(userData.isAdmin){
	return Product.find().then(result => {
		return response.send(result)
		}).catch(error =>{
		console.log(error)
		})
	}
	else{
		return response.send("You are not an admin")
	}
}

module.exports.retrieveProduct = (request, response) =>{
	let productId = request.params.productId;
	return Product.findById(productId).then(result => {return response.send(result)
	})
}

module.exports.updateProduct = (request, response) => {
	const userData = auth.decode(request.headers.authorization)
	let productId = request.params.productId;

		let updatedProduct = {
		name: request.body.name,
		description: request.body.description,
		price: request.body.price,
		stock: request.body.stock,
	}
	if(userData.isAdmin){
		return Product.findByIdAndUpdate(productId, updatedProduct, {new: true}).then(result => {response.send(result)
		}).catch(error => console.log(error))
	}
	else{
		return response.send("You are not an admin")
	}
}

module.exports.archiveProduct = (request, response) => {
	const userData = auth.decode(request.headers.authorization)
	let productId = request.params.productId;

	if(userData.isAdmin){
		return Product.findById(productId).then(doc =>{
		if(doc.isActive){
			return Product.findByIdAndUpdate(productId, {isActive: false}, {new:true}).then(result => {
			return response.send(result)
			}).catch(error => console.log(error))
		}
		else{
			return Product.findByIdAndUpdate(productId, {isActive: true}, {new:true}).then(result => {
			return response.send(result)
			}).catch(error => console.log(error))
			}
		})
	}
	else{
		return response.send("You are not an admin")
	}
}


