const User = require("../models/User");
const Product = require("../models/Product");

const bcrypt = require("bcrypt")
const auth = require("../auth")


module.exports.registerUser = (request, response) =>{
	let newUser = new User({
		username: request.body.username,
		email: request.body.email,
		password: bcrypt.hashSync(request.body.password, 10),
		mobileNo: request.body.mobileNo
	})
	return User.find({email: request.body.email}).then(result =>{
		console.log(request.body.email)

		if(result.length > 0){
			return response.send("This email has been taken")
		}
		else {
			return newUser.save().then(user =>{
				console.log(user)
				response.send(`Congratutions, ${newUser.email}! has been registered.`)
			})
		}
	}).catch(err => {console.log(err)
		response.send(false)})
	}

module.exports.loginUser = (request, response) =>{
	return User.findOne({email: request.body.email}).then(result =>{
		if(result === null){
			response.send(`Your email: ${request.body.email} is not yet registered. Register first!`)
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);
				if(isPasswordCorrect){
					let token =  auth.createAccessToken(result)
					console.log(token)
					return response.send({accessToken: token});
				}
				else{
					return response.send(`Incorrect password, please try again!`);
				}
		}
	}).catch(err => {console.log(err)
		response.send(false)})
}

module.exports.updateRole = (request, response) =>{
	const userData = auth.decode(request.headers.authorization);
	let idToBeUpdated = request.params.userId; 

	if(userData.isAdmin){
		return User.findById(idToBeUpdated).then(result =>{
			let update = {
				isAdmin: !result.isAdmin
			}
			
			return User.findByIdAndUpdate(idToBeUpdated,update, {new: true}).then(document =>
				response.send(document))	
		}).catch(err => response.send(err))
			}
	else{
		return response.send("You don't have access on this page!")
	}
}


module.exports.checkOut = async (request, response) => { 
	const userData = auth.decode(request.headers.authorization);
	const productId = request.params.productId

	if(!userData.isAdmin){

		const product = await Product.findById(productId);
		let totalAmount = product.price * request.body.quantity;


		let user = await User.findById(userData.id).then(result => {result.orders.push({
			productName: product.name,
			quantity: request.body.quantity,
			totalAmount: totalAmount})
			return result.save().then(document => {
				return true
			})
		}).catch(err => err)

		let putOrderId = await Product.findById(productId).then(res => {res.orders.push({
		userId: userData.id
		})
		res.stock -= request.body.quantity;
		return res.save().then(success => {
			console.log(success)
			return true });
		}).catch(err => err)

		return (user && putOrderId) ? response.send("Checkout successfully!") : response.send("We encountered an error ");

	}
	else{
		return response.send("You are an admin")
	}
}






/*module.exports.addToCart = async (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const productId = request.params.productId


		if(!userData.isAdmin){
		const product = await Product.findById(productId);

		const user = await User.findById(userData.id).then(result => {result.carts.push({
			productName: product.name,
			price: product.price})

		return result.save().then(document =>{
			return response.send("Successfully added to cart! ")})
		})
	}
		else{
			return response.send("You are an admin")
		}
}
*/




/*module.exports.addToCart = async (request, response) => { 
	const userData = auth.decode(request.headers.authorization);
	const productId = request.params.productId

	if(!userData.isAdmin){

		const product = await Product.findById(productId);
		let totalAmount = product.price * request.body.quantity;


		let user = await User.findById(userData.id).then(result => {result.orders.push({
			productName: product.name,
			quantity: request.body.quantity,
			totalAmount: totalAmount})
			return result.save().then(document => {
				return response.send("Successfully added to Cart")
			})
		}).catch(err => err)
	}
	else {
		return response.send("You are an admin")
	}
}

module.exports.checkOut = async (request, response) =>{
	const userData = auth.decode(request.headers.authorization)
	const productId = request.params.productId

if(!userData.isAdmin){

	let putOrderId = await Product.findById(productId).then(res => {res.orders.push({
	userId: userData.id
	})
	res.stock -= request.body.quantity;
	return res.save().then(success => {
		return response.send("checkOut Successfully!")});
	}).catch(err => err)

}
	else{
		return response.send("You are an admin")
	}
}*/
